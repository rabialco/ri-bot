package com.bot.ribot.handler.state;

import com.bot.ribot.handler.message.Messages;
import com.bot.ribot.model.LineUser;
import com.bot.ribot.repository.LineUserRepository;
import com.bot.ribot.repository.MatchSessionRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

public class ChooseLocationStateTest {
    @InjectMocks
    private ChooseLocationState chooseLocationState;

    private String responses;

    @Mock
    LineUserRepository lineUserRepository;

    @Mock
    MatchSessionRepository matchSessionRepository;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        LineUser userQra = new LineUser("<3", "Qra");
        //MatchSession match = new MatchSession(userQra, "Tinju");
        userQra.setState(ChooseLocationState.DB_COL_NAME);
        when(lineUserRepository.findLineUserByUserId("<3")).thenReturn(
            userQra
        );
//        when(matchSessionRepository.findMatchSessionAfterChooseGame("<3")).thenReturn(
//                match
//        );
    }
    
    @Test
    public void contextLoads() {
        assertThat(chooseLocationState).isNotNull();
    }

    @Test
    public void registerTest() {
        responses = Messages.ALREADY_REGISTERED;
        assertEquals(responses, chooseLocationState.register("210", "Qra"));
    }

    @Test
    public void makeSessionTest() {
        responses = Messages.CHOOSE_LOCATION_WRONG_COMMAND;
        assertEquals(responses, chooseLocationState.makeSession("1810"));
    }

    @Test
    public void acceptMatchTest() {
        responses = Messages.ERROR_ACCEPTING_MATCH_WHEN_CREATE_MATCH;
        assertEquals(responses, chooseLocationState.acceptMatch("<3", "tes"));
    }

    // @Test
    // public void othersTest() {
    //     responses = Messages.CHOOSE_LOCATION_SUCCESS;
    //     assertEquals(responses, chooseLocationState.others("<3", "madura"));
    // }
}