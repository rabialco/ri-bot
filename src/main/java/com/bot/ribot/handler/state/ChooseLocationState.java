package com.bot.ribot.handler.state;

import com.bot.ribot.handler.message.Messages;
import com.bot.ribot.model.LineUser;
import com.bot.ribot.model.MatchSession;
import org.springframework.stereotype.Component;

@Component
public class ChooseLocationState extends State {
    public static final String DB_COL_NAME = "CHOOSE_LOCATION";

    public String register(String userId, String displayName) {
        return Messages.ALREADY_REGISTERED;
    }

    /**
    * Handle when user's input /makesession command.
    */
    public String makeSession(String userId) {
        return Messages.CHOOSE_LOCATION_WRONG_COMMAND;
    }

    public String acceptMatch(String userId, String command) {
        return Messages.ERROR_ACCEPTING_MATCH_WHEN_CREATE_MATCH;
    }

    /**
     * Handle when user's want to tell the location she/he want to play.
     */
    public String others(String userId, String command) {
        try {
            LineUser user = lineUserRepository.findLineUserByUserId(userId);
            MatchSession match = matchSessionRepository.findMatchSessionAfterChooseGame(userId);
            user.setState(ChooseTimeState.DB_COL_NAME);
            match.setGamePlace(command);
    
            lineUserRepository.save(user);
            matchSessionRepository.save(match);
            return Messages.CHOOSE_LOCATION_SUCCESS;
        } catch (Exception e) {
            return e.toString();
        }
    }
}