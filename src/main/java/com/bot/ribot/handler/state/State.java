package com.bot.ribot.handler.state;

import com.bot.ribot.handler.message.Messages;
import com.bot.ribot.model.LineUser;
import com.bot.ribot.model.MatchSession;
import com.bot.ribot.repository.LineUserRepository;
import com.bot.ribot.repository.MatchSessionRepository;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.PushMessage;
import com.linecorp.bot.model.message.TextMessage;
import java.text.ParseException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;



public abstract class State {
    @Autowired
    LineMessagingClient lineMessagingClient;
    @Autowired
    LineUserRepository lineUserRepository;
    @Autowired
    MatchSessionRepository matchSessionRepository;

    public abstract String register(String userId, String displayName);
    
    public abstract String makeSession(String userId);

    public abstract String others(String userId, String command) throws ParseException;

    public abstract String acceptMatch(String userId, String command);

    /**
     * this is function to blast notification to active user
     * when match session are made.
     * TO DO: Move to observer pattern
     */
    public void notifyActiveUser(MatchSession match) {
        TextMessage text = new TextMessage("paling awal notify di state");
        lineMessagingClient.pushMessage(new PushMessage("U736daa71fa827df41b58e025e71dbc44", text));
        List<LineUser> lineUsers = lineUserRepository.findAllLineUser();
        String greetings = "Telah dibuat match baru:\n";
        
        text = new TextMessage("mau masuk foor loop");
        lineMessagingClient.pushMessage(new PushMessage("U736daa71fa827df41b58e025e71dbc44", text));
        for (LineUser lineUser : lineUsers) {
            Boolean userGetNotification = lineUser.getGetNotification();
            if (userGetNotification) {
                TextMessage textMessage = new TextMessage(greetings + match.toString());
                lineMessagingClient.pushMessage(new PushMessage(lineUser.getUserId(), textMessage));
            }
        }
    }

    /**
     * Handle when user's want to remind the rival.
     */
    public String remindRival(String userId) {
        LineUser user = lineUserRepository.findLineUserByUserId(userId);
        //LineUser others = lineUserRepository.findRivalByUserId(userId);

        //tambahin fungsi buat kirim pesan ke rival (pakenya push messages)
        return Messages.REMIND_SUCCESSFUL;
    }

    /**
     * Handle to toggle and Get Notification.
     */
    public String toggleGetNotification(String userId) {
        LineUser user = lineUserRepository.findLineUserByUserId(userId);
        String tambahan = " ";
        Boolean getNotification = user.getGetNotification();

        if (getNotification) {
            tambahan = " tidak";
        }
        
        user.setGetNotification(!getNotification);
        return Messages.TOGGLE_SUCCESSFUL + tambahan + " akan mendapatkan notifikasi";
    }
}