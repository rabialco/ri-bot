package com.bot.ribot.handler.state;

import com.bot.ribot.handler.message.Messages;
import com.bot.ribot.model.LineUser;
import com.bot.ribot.model.MatchSession;
import com.bot.ribot.repository.LineUserRepository;
import com.linecorp.bot.model.PushMessage;
import com.linecorp.bot.model.message.TextMessage;
import org.springframework.stereotype.Component;

@Component
public class ActiveState extends State {
    public static final String DB_COL_NAME = "ACTIVE";

    public String register(String userId, String displayName) {
        return Messages.ALREADY_REGISTERED;
    }

    /**
     * Handle when user's want to create a game.
     */
    public String makeSession(String userId) {
        LineUser user = lineUserRepository.findLineUserByUserId(userId);
        user.setState(ChooseGameState.DB_COL_NAME);
        lineUserRepository.save(user);

        StringBuilder messages = new StringBuilder();
        messages.append("Silahkan pilih game yang ingin anda mainkan :");
        for (String game : Messages.availableGame) {
            messages.append("\n");
            messages.append(game);
        }
        return messages.toString();
    }

    /**
     * Handle when user's want to accept a invitation to a game.
     */
    public String acceptMatch(String userId, String command) {
        int matchId;
        boolean ada = false;
        try {
            matchId = Integer.parseInt(command);
        } catch (Exception e) {
            return Messages.ERROR_MATCH_ID_NOT_INTEGER;
        }

        try {
            for (MatchSession i : matchSessionRepository.findAllMatchSession()) {
                if (i.getUserFinder().getUserId().equals(i.getUserRival().getUserId())
                        && i.getGameType() != null && i.getMatchId() == matchId) {
                    ada = true;
                    break;
                }
            }

            if (!ada) {
                return Messages.ERROR_MATCH_ID_NOT_AVAILABLE;
            } else {
                MatchSession match = matchSessionRepository.findSessionByMatchId(matchId);
                LineUser userFinder = match.getUserFinder();
                LineUser userRival = lineUserRepository.findLineUserByUserId(userId);
                
                userFinder.setState(PassiveState.DB_COL_NAME);
                userRival.setState(PassiveState.DB_COL_NAME);
                match.setUserRival(userRival);
    
                matchSessionRepository.save(match);
                lineUserRepository.save(userRival);
                lineUserRepository.save(userFinder);

                TextMessage text = new TextMessage("Permainan yang anda buat dengan matchId "
                        + match.getMatchId()
                    + " berhasil diterima oleh user " + userRival.getDisplayName() + "\n\n"
                    + " Untuk menyelesaikan permainan gunakan /finish");
                lineMessagingClient.pushMessage(new PushMessage(userFinder.getUserId(), text));
                return Messages.ACCEPTING_MATCH_SUCCESSFUL;
            }
        } catch (Exception e) {
            return e.toString();
        }
    }

    public String others(String userId, String command) {
        return Messages.ACTIVE_STATE_WRONG_COMMAND;
    }
    



}