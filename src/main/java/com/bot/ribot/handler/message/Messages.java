package com.bot.ribot.handler.message;

import com.bot.ribot.model.MatchSession;
import java.util.ArrayList;
import java.util.Arrays;


public class Messages {
    /**
     * check whether the string is a valid game or not.
     * return true if a valid game, false if others.
     */
    public static boolean isAvailableGame(String check) {
        for (String game : availableGame) {
            if (game.equalsIgnoreCase(check)) {
                return true;
            }
        }
        return false;
    }
    //semua string cetak cetak simpan disini biar rapi

    public static final ArrayList<String> availableGame = new ArrayList<String>(
            Arrays.asList("Tenis Meja", "Catur", "Tinju", "Dota 1vs1", "Mobile Legends 1vs1")
    );

    public static final String SHOW_SUMMARY_MESSAGE = "Summary sedang ditunjukkan!\n"
            + "Maaf command ini masih dalam tahap pengembangan";

    public static final String SHOW_MENU_MESSAGE = "Command Menu :\n1. /register\n2. /makeSession\n"
            + "3. /remindRival\n4. /showSummary\n5. /showAvailableSession\n6. /toggleNotification\n"
            + "7. /accept [matchId] -- (tanpa kurung siku)\n\n"
            + "Untuk memunculkan menu ini lagi ketik: \n/showMenu";

    public static final String ALREADY_REGISTERED = "Anda sudah terdaftar di Ri-Bot. "
            + "Silahkan masukkan perintah lain";

    public static final String ACTIVE_STATE_WRONG_COMMAND = "Perintah yang anda masukkan salah. "
            + "Silahkan buat game dengan perintah /makeSession";
    
    public static final String CHOOSE_GAME_SUCCESS =
            "Silakan masukkan lokasi tempat anda ingin bermain";

    public static final String CHOOSE_TIME_WRONG_COMMAND = "Perintah yang anda masukkan salah."
            + " Silahkan pilih waktu bermain";

    public static final String CHOOSE_LOCATION_SUCCESS = 
            "Silahkan masukkan waktu bermain anda dengan format dd-MM-yyyy hh:mm";

    public static final String CHOOSE_LOCATION_WRONG_COMMAND = "Perintah yang anda masukkan salah."
            + " Silahkan pilih lokasi tempat anda akan bermain";

    public static final String CHOOSE_TIME_SUCCESS = "Permainan berhasil dibuat. "
            + "gunakan command /finish untuk menyelesaikan permainan";

    public static final String PASSIVE_STATE_WRONG_COMMAND = "Perintah yang anda masukkan salah. "
            + "Silahkan selesaikan permainan menggunakan command /finish";

    public static final String PASSIVE_STATE_SUCCESS = "Permainan telah diselesaikan. "
            + "Silahkan gunakan command /makeSession untuk membuat permainan baru";
    
    public static final String SUCCESSFULLY_REGISTERED = "Registrasi sukses. "
            + "Silahkan masukkan command /makeSession untuk membuat permainan baru";

    public static final String UNREGISTERED_WRONG_COMMAND = "Anda belum terdaftar pada"
            + " sistem Ri-Bot. Silahkan masukkan perintah /register untuk mendaftar";
    
    public static final String REMIND_SUCCESSFUL = "Rival anda sudah diingatkan "
            + "terkait game anda. "
            + "Rival akan diingatkan kembali setiap 10 menit";

    public static final String TOGGLE_SUCCESSFUL = "Toggle notifikasi berhasil"
            + "anda saat ini";
    
    public static final String ACCEPTING_MATCH_SUCCESSFUL = "Anda berhasil" 
            + "menerima permainan. Untuk menyelesaikan permainan gunakan /finish";

    public static final String ERROR_MATCH_ID_NOT_INTEGER = "Format MatchId Salah." 
            + "Silahkan masukkan matchId berupa integer";
    
    public static final String ERROR_MATCH_ID_NOT_AVAILABLE = "MatchId yang "
            + "anda masukkan salah. Silahkan masukkan matchId yang valid";
    
    public static final String ERROR_ACCEPTING_MATCH_WHEN_CREATE_MATCH = "Anda tidak "
            + "dapat menerima permainan saat membuat permainan";

    /**
    * Method for print summary after new match has been
    * created succesfully.
    */
    public static final String createMatchSuccess(MatchSession newMatch) {
        return "Permainan berhasil dibuat!" + "\n" + "\n"
                + "Tipe game: " + newMatch.getGameType() + "\n"
                + "Waktu main: " + newMatch.getGameTime() + "\n"
                + "Lokasi: " + newMatch.getGamePlace() + "\n" + "\n"
                + "Silakan menunggu ada user yang menerima permainan anda.";
    }

}